from random import randrange
from copy import deepcopy
from timeit import default_timer
    

#implementation from https://www.geeksforgeeks.org/merge-sort/
#only changes were renamed variables
def Merge_Sort(array: list) -> None:
    if len(array) > 1:
        mid_pt = len(array)//2

        left_arr = array[:mid_pt]
        right_arr = array[mid_pt:]

        Merge_Sort(left_arr)
        Merge_Sort(right_arr)

        i = j = k = 0
 
        while i < len(left_arr) and j < len(right_arr):
            if left_arr[i] <= right_arr[j]:
                array[k] = left_arr[i]
                i += 1
            else:
                array[k] = right_arr[j]
                j += 1
            k += 1
 
        # Checking if any element was left
        while i < len(left_arr):
            array[k] = left_arr[i]
            i += 1
            k += 1
 
        while j < len(right_arr):
            array[k] = right_arr[j]
            j += 1
            k += 1

# implementation from https://www.geeksforgeeks.org/insertion-sort/
# only significant alteration is the if statement before the for loop
def Insertion_Sort(array: list) -> None:
    if len(array) > 1:   
        for i in range(1, len(array)):
            key = array[i]

            #shift elements of array[0...i-1], that are greater 
            # than key to one position ahead of their current position
            j = i-1
            while j >= 0 and key < array[j]:
                array[j + 1] = array[j]
                j -= 1
            
            array[j+1] = key

# uses combination of above functions
def Tim_Sort(array: list, k: int) -> None:
    if len(array) == 1:
        return
    if len(array) <= k:
        Insertion_Sort(array)
    else:
        mid_pt = len(array)//2

        left_arr = array[:mid_pt]
        right_arr = array[mid_pt:]

        Tim_Sort(left_arr, k)
        Tim_Sort(right_arr, k)

        i = j = k = 0
 
        while i < len(left_arr) and j < len(right_arr):
            if left_arr[i] <= right_arr[j]:
                array[k] = left_arr[i]
                i += 1
            else:
                array[k] = right_arr[j]
                j += 1
            k += 1
 
        # Checking if any element was left
        while i < len(left_arr):
            array[k] = left_arr[i]
            i += 1
            k += 1
 
        while j < len(right_arr):
            array[k] = right_arr[j]
            j += 1
            k += 1

if __name__ == "__main__":
    #Have user input # of trials to run for each list size. Default to 10000
    print("\n\nEnter # of trials: ", end='')
    Trials = input()
    if Trials.isnumeric():
        Trials = int(Trials)
    else:
        Trials = 10000

    #Have user provide random values. If none or invalid values are provided, set values to 0, 10000
    print("\nEnter the range of random values in the format of 'lower_bnd upper_bnd' (space deliniated): ", end='')
    rand_range = input()
    print("\n\n*******************************************\n")

    #Set range of random values that can be generated for the list
    rand_range = rand_range.split(' ')
    if (len(rand_range) == 2 and rand_range[0].isnumeric()):
        lower_bnd = int(rand_range[0])
    else:
        lower_bnd = 0
    if (len(rand_range) == 2 and rand_range[1].isnumeric()):
        upper_bnd = int(rand_range[1])
    else:
        upper_bnd = 10000

    run_insert_sort = "no"
    run_merge_sort = "no"
    run_tim_sort = "no"

    while run_insert_sort == run_merge_sort == run_tim_sort == "no":
        # Ask if user wants to run insertion sort during the tests
        print("\nRun Insertion Sort? enter 'yes' or 'no': ", end='')
        run_insert_sort = input()
        while (type(run_insert_sort) is not str or (run_insert_sort != 'yes' and run_insert_sort != 'no')):
            print("invalid input. Enter 'yes' or 'no': ", end='')
            run_insert_sort = input()

        #Ask if user wants to run Merge Sort during the tests
        print("\nRun Merge Sort? enter 'yes' or 'no': ", end='')
        run_merge_sort = input()
        while (type(run_merge_sort) is not str or (run_merge_sort != 'yes' and run_merge_sort != 'no')):
            print("invalid input. Enter 'yes' or 'no': ", end='')
            run_merge_sort = input()

        #Ask if user wants to run tim sort
        print("\nRun Tim Sort? enter 'yes' or 'no': ", end='')
        run_tim_sort = input()
        while (type(run_tim_sort) is not str or (run_tim_sort != 'yes' and run_tim_sort != 'no')):
            print("invalid input. Enter 'yes' or 'no': ", end='')
            run_tim_sort = input()
        
        if run_insert_sort == run_merge_sort == run_tim_sort == "no":
            print("\nYou must run at least one of the algorithms, try again\n")

    #variable that keeps track if the user wants to run another experiment
    user_input = "yes"
    while (user_input == "yes"):        
        print("\nEnter size of the list: ", end='')
        N = input()
        while N.isnumeric() is False:
            print("\nInvalid list size, try again: ", end='')
            N = input()
        N = int(N)

        if (run_tim_sort == "yes"):
            print("\nEnter threshold for insertion_sort (for tim_sort). If size of the list is less or equal to this value, insertion sort is run; else, merge sort: ", end='')
            K = input()
            while K.isnumeric() is False:
                print("\nInvalid K value, try again: ", end='')
                K = input()
            K = int(K)

        insert_time_cummulative = 0
        merge_time_cummulative = 0
        tim_time_cummulative = 0

        for i in range(Trials):
            template_list = [randrange(lower_bnd, upper_bnd) for i in range(N)]                

            if (run_insert_sort == "yes"):
                insert_list = deepcopy(template_list)
                print("\ninsert list (pre): ", insert_list)
                start = default_timer()
                Insertion_Sort(insert_list)
                insert_time_cummulative += default_timer() - start
                print("\ninsert list (post): ", insert_list)

            if (run_merge_sort == "yes"):
                merge_list = deepcopy(template_list)
                print("\nmerge list (pre): ",merge_list)
                start = default_timer()
                Merge_Sort(merge_list)
                merge_time_cummulative += default_timer() - start
                print("\nmerge list (post): ", merge_list)

            if (run_tim_sort == "yes"):
                tim_list = deepcopy(template_list)                
                print("\ntim_sort list (pre): ", tim_list)
                start = default_timer()
                Tim_Sort(tim_list, K)
                tim_time_cummulative += default_timer() - start
                print("tim_sort list (post): ", tim_list)

        if run_insert_sort == "yes":
            #print("\n", insert_list)
            print("\ncumulative insert_sort runtime: ", insert_time_cummulative)
            print("average insert_sort runtime: ", insert_time_cummulative/Trials)

        if run_merge_sort == "yes":
            #print("\n", merge_list)
            print("\ncumulative merge_sort runtime: ", merge_time_cummulative)
            print("average merge_sort runtime: ", merge_time_cummulative/Trials)

        if (run_tim_sort == "yes"):
            print("\ncummulative tim_sort runtime: ", tim_time_cummulative)
            print("average tim_sort runtime: ", tim_time_cummulative/Trials)

        print("\n\nrun another test? Enter 'yes' or 'no': ", end='')
        user_input = input()
        while (user_input != "yes" and user_input != "no"):
            print("invalid input. enter 'yes' or 'no': ", end='')
            user_input = input()
        print("\n\n*******************************************\n")
