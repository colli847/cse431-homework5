#include <ctime>
using std::clock_t; using std::clock;

#include <iostream>
using std::cout; using std::cin; using std::endl; using std::flush;

#include <iomanip>
using std::setprecision; using std::fixed;

#include <string> 
using std::string;

#include <map>
#include <unordered_map>
using std::multimap; using std::unordered_multimap;

#include <cstdlib>
using std::rand;

int main() 
{
    int Trials = 1000;
    /*cout << "\n***********\n" <<
        "Enter the number of Trials to run per test" << endl;
    cin >> Trials;
    */


    int lower_bnd = 0, upper_bnd = 10000;
    /*cout << "\n Enter the range of random values in the format of lower_bnd upper_bnd (space deliniated): " << flush;
    cin >> lower_bnd >> upper_bnd;
    */

    clock_t start_tree_time;
    clock_t start_hash_time;
    clock_t stop_tree_time;
    clock_t stop_hash_time;

    int dictionary_size; 
    clock_t multimap_cumm_time = 0;
    clock_t unordered_cumm_time = 0;

    int key, value;

    unordered_multimap<int, int> hash_dict;
    multimap<int, int> tree_dict;

    string user_input = "yes";
    do 
    {
        cout << "\n\nEnter size of dictionary: " << flush;
        cin >> dictionary_size;

        
        
        for (int j = 0; j < Trials; ++j)
        {
            for (int i = 0; i < dictionary_size; ++i)
            {
                key = (rand() % upper_bnd) + lower_bnd;
                value = (rand() % upper_bnd) + lower_bnd;

                start_hash_time = clock();
                hash_dict.insert({key, value});
                stop_hash_time = clock();
                
                start_tree_time = clock();
                tree_dict.insert({key, value});
                stop_tree_time = clock();

                multimap_cumm_time += stop_tree_time - start_tree_time;
                unordered_cumm_time += stop_hash_time - start_hash_time;
            }
        }
        
        std::cout << fixed << setprecision(10) << "\n\nmultimap cumm time: " << static_cast<double>(multimap_cumm_time) / CLOCKS_PER_SEC << endl;
        std::cout << fixed << setprecision(10) << "multimap avg time per trial: " << (static_cast<double>(multimap_cumm_time) / CLOCKS_PER_SEC) / Trials << endl;

        std::cout << fixed << setprecision(10) << "\nunordered multimap cumm time: " << static_cast<double>(unordered_cumm_time) / CLOCKS_PER_SEC << endl;
        std::cout << fixed << setprecision(10) << "unordered multimap avg time per trial: " << (static_cast<double>(unordered_cumm_time) / CLOCKS_PER_SEC) / Trials << endl;


        cout << "Run another Test? enter yes or no: " << flush;
        cin >> user_input;

        multimap_cumm_time = 0; unordered_cumm_time = 0;
        hash_dict.clear(); tree_dict.clear();

    } while(user_input == "yes");
}